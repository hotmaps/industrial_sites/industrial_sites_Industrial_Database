[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4687147.svg)](https://doi.org/10.5281/zenodo.4687147)

# Industrial Sites: Industrial Database for EU28 + Norway 

In this repository are over 5000 georeferenced industrial sites of energy-intensive industry sectors published, together with GHG-emissions, 
production capacity, fuel demand and excess heat potentials calculated from emission and production data.

## Repository structure

Files:
```
data/Industrial_Database.csv                                -- Tabular industrial sites, georeferenced
datapackage.json                                            -- Datapackage JSON file with the main meta-data

```

## Documentation
The dataset in the folder "industrial_sites" analyses the industrial Heating and Cooling energy demand and comprises of three mail elements:

**Table 1.** Characteristics of data provided within Task 2.4 Industrial Processes.
<table>
   <tr>
    <td>Task</td>
    <td>Dataset</td>
  </tr>
  <tr>
    <td>1. Performance and cost data of industrial steam and district heating generation technologies</td>
    <td>CAPEX, OPEX, Lifetime, ThermalEfficiency, PowertoHeatRatio</td>
  </tr>
  <tr>
    <td>2. Benchmarks on H&C Final Energy Consumption (FEC) and excess heat potentials for industrial processesIndustryBenchmarks</td>
    <td>IndustryBenchmarks</td>
  </tr>
  <tr>
    <td>3. Industrial plants FEC and excess heat potentials</td>
    <td>IndustrialDatabase</td>
  </tr>
</table>

The datasets are to be found in the respective repository.

## Description of the task
The overall aim of this task is to derive a georeferenced default dataset of energy-intensive industries, including emissions, 
processes, production capacities as well as FEC of each site. 

### Methodology

The approach presented here uses the georeferenced emission data of energy intensive processes for estimation of fuel and 
electricity demand and excess heat potentials on different temperature levels. 
For this purpose, several available databases were matched, e.g. ETS (European Emission Trading System), E-PRTR (European Pollutant Release and Transfer Register) 
and sectoral databases (glass, cement, steel, paper etc.), using an algorithm which considers company name, location and activity. 
With this approach, georeferenced production capacity is made accessible for energy intensive industry sectors in EU28. 
As the data from commercial databases cannot be published site-specific due to license restrictions, the retrieved data about production capacities is aggregated at
country level and then broken down on individual industrial sites using the emission data as a distribution key. 
With the development of a generic database with specific FEC per produced tonne of a specific product, the fuel and electricity demand can be derived for each process. 
A comparison is included of those approaches to estimate the deviations included in the open data set.

<table>
  <tr>
    <td>Spatial resolution</td>
    <td>Temporal resolution</td>
  </tr>
  <tr>
    <td>Coordinates</td>
    <td>yearly (2014)</td>
  </tr>
</table>

### Limitations of data

The calculation of annual production or FEC from emissions is a valid approach if no production data are available, but includes more uncertainty 
as the calculation of UED of industrial process as it is better correlated to the physical production than emissions, even though they are a good indicator, 
as shown by the emission factors in this study. Also, the different measurement methods and system boundaries were identified as uncertainties.
Tackling the excess heat potentials in Europe requires the inclusion of more subsectors. Furthermore, to assess the potential for the new generation of 
DH networks with low temperatures and the combined use with HPs, excess heat potentials below 100°C can be included as well. The technical and economic 
potential for the utilization of excess heat requires the analysis of temperature profiles of each process, the mapping with UED for heat of different 
temperature levels as well as the development of scenarios of future demand and supply.

## Projection

The coordinates in the database can be projected with:  
Geographic Coordinate System:	GCS_WGS_1984  
Datum: 	D_WGS_1984  
Prime Meridian: 	Greenwich  
Angular Unit: 	Degree  

## References
 
[1]	EU-ETS (Emission Trading System), published by the European Commission, 2014.       
[2]	E-PRTR (European Pollutant Release and Transfer Register), published by the EEA, 2015.  
[3]	Steel database (VDeH Steel)   
[4] RISI Pulp and Paper  
[5] GlassGlobal  
[6] Cement Directory

## How to cite

Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Mueller (e-think), Michael Hartner (TUW), Tobias Fleiter, Anna-Lena Klingler, Matthias Kuehnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW)
Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 [www.hotmaps-project.eu](https://www.hotmaps-project.eu/d2-3-wp2-report-open-data-set-eu28/) 

### Authors
Pia Manz, Tobias Fleiter <sup>*</sup>

<sup>*</sup> [Fraunhofer Institute for Systems and Innovation Research](https://www.isi.fraunhofer.de/en.html)  
Breslauer Str. 48  
D-76139 Karlsruhe

### License

Copyright © 2016-2018: Pia Manz, Tobias Fleiter
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html

## Acknowledgement

We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) 
(Grant Agreement number 723677), which provided the funding to carry out the present investigation.


